# Resume Website

Repository hosting my resume site source code. LaTeX PDF source is located in [Kovrinic/latex-resume](https://gitlab.com/Kovrinic/latex-resume).


## HTTPS

#### LetsEncrypt

Initiate [LetsEncrypt](https://letsencrypt.org/) manual DNS validation method:

```bash
sudo letsencrypt --text --agree-tos --email certbot@matthewrothfuss.me -d matthewrothfuss.me --manual --preferred-challenges dns --expand --renew-by-default --manual-public-ip-logging-ok certonly
```

Need to read into [`--renew-by-default`](https://certbot.eff.org/docs/using.html#certbot-command-line-options) before next SSL cert renewal.  `letsencrypt` may be `certbot` for other systems, depends on which was installed.

`certbot` will output the challenge and wait until the user proceeds to the next step.


#### Namecheap

Add the `challenge_key` that `certbot` output into a DNS TXT record where our domain is hosted _([Namecheap/.../matthewrothfuss.me/advancedns](https://ap.www.namecheap.com/Domains/DomainControlPanel/matthewrothfuss.me/advancedns))_:

| **Type** | **Host** | **Value** | **TTL** |
|--|--|--|--|
| TXT Record | _acme-challenge | `challenge_key` | 1 min |

Double check that the DNS has populated changes _(should see the TXT response within the ANSWER section)_:

```bash
dig -t txt _acme-challenge.matthewrothfuss.me
```

If it's a success, then hit `enter` in the initial step and let `certbot` finish setting up the `pem` files.


#### GitLab

Add `fullchain.pem` _(Certificate)_ and `privkey.pem` _(Key)_ to [pages/domains/matthewrothfuss.me/edit](https://gitlab.com/Kovrinic/kovrinic.gitlab.io/pages/domains/matthewrothfuss.me/edit):

```bash
# fullchain.pem == certificate
sudo cat /etc/letsencrypt/live/matthewrothfuss.me/fullchain.pem|xclip -i -selection clipboard
# privkey.pem == key
sudo cat /etc/letsencrypt/live/matthewrothfuss.me/privkey.pem|xclip -i -selection clipboard
```

Copy & paste into GitLab.  Local browser cache may need to be cleared to see changes immediately.  Can now remove DNS TXT record from Namecheap, as it was only needed during the initial validation.


## Sources
- _Using [Ceevee](https://www.styleshout.com/free-templates/ceevee/) template._
- _Copy of original code: [tree/ceevee](https://gitlab.com/Kovrinic/kovrinic.gitlab.io/tree/ceevee)._

